Test app with yandex maps: Yandex Maps.

1. Used drop down field to search address.
2. Addesses are avaliable as points on map
3. Point can be moved
4. Route is build btw points
5. Ability to use drag-and-drop to rebuild route list
6. Balloon marker when was clicked
7. Rebuilding routes when dragging

### Before running project:
`npm install` - to install all required dependencies

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.