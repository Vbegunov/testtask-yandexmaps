import React, { Component }         from 'react';
import { YMaps, Map }               from 'react-yandex-maps';
import DropDownList                 from './DropDownList.react.js';
import Point                        from './Point.react';
import Line                         from './Line.react';
import { DragDropContextProvider }  from 'react-dnd';
import { DragSource }               from 'react-dnd';
import { DropTarget }               from 'react-dnd';
import HTML5Backend                 from 'react-dnd-html5-backend'
import { get }                      from './utils/request';
// styles
import './App.scss';

const source = {
  beginDrag(props, monitor, component) {
    return {
      indexFrom: props.index,
    };
  },

  endDrag(props, monitor, component) {
    if (!monitor.didDrop()) {
      return;
    }

    const item = monitor.getItem();
    const dropResult = monitor.getDropResult();
    props.replacePoints(item.indexFrom, dropResult.indexTo);
  }
};

const collectSource = (connect, monitor) => {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.getItem()
  };
}

const target = {
  drop(props, monitor, component) {
    if (monitor.didDrop()) {
      return;
    }
    return  {indexTo: props.index};
  }
};


const collectTarget = (connect, monitor) => {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop(),
    itemType: monitor.getItemType(),
  };
}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      routeList:      [],
      points:         [],
      lines:          [],
      searchResults:  [],
      searchKey:      [],
      defaultPos:     [55.75, 37.57],
      loaded:         false,
    };

    this.selectPoint = this.selectPoint.bind(this);
    this.searchPoint = this.searchPoint.bind(this);
    this.handleDrag = this.handleDrag.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this);
    this.onClose = this.onClose.bind(this);
    this.removePoint = this.removePoint.bind(this);
    this.replacePoints = this.replacePoints.bind(this);
    this.setLoaded = this.setLoaded.bind(this);
  }

  selectPoint(item) {
    this.setState(state => {
      const point = item.GeoObject.Point.pos.split(' ').map(item => parseFloat(item)).reverse();
      this.state.defaultPos = point;
      return {
        points: [
          ...state.points, ...[{point, GeoObject: item.GeoObject}]
        ],
      }
    });
  }

  searchPoint(value) {
    if(value && !this.timeout) {
     this.timeout = setTimeout(() => {
        this.state.searchKey = value;
        get({
          url: 'https://geocode-maps.yandex.ru/1.x/',
          parameters: {
            format:   'json',
            apikey:   '52f1bc53-23e5-4d43-b685-d464dc3a791e',
            geocode:  value
          }
        }).then(data => {
          this.setState({searchResults: data.response.GeoObjectCollection.featureMember});
          this.timeout = null;
        });
      }, 50);
    }
  }

  handleDrag(pointKey, e) {
    if(e && e._sourceEvent) {
      let newCoords = e._sourceEvent.originalEvent.target.geometry._coordinates;
      this.setState(state => {
        state.points[pointKey].point = newCoords;
        return {
          points: state.points.map(point => point), 
        }
      })
    }
  }

  onDragEnd() {
    this.forceUpdate();
  }

  onClose() {
    this.setState({searchResults: []});
  }

  removePoint(pointKey) {
    this.setState(state => {
      let newPoints = this.state.points.filter((item, key) => {
        if(key === pointKey) return false;
        else return true;
      });
      this.setState({points: newPoints});
    });
  }

  replacePoints(indexFrom, indexTo) {
    let temp = this.state.points[indexFrom];

    this.state.points[indexFrom] = this.state.points[indexTo];
    this.state.points[indexTo] = temp;
    this.setState(state => {
      return {
        points: [...state.points],
      }
    });
  }

  setLoaded() {
    this.setState({loaded: true});
  }

  componentWillMount() {
  }

  render() {
    const { points, loaded } = this.state;
    let Items = this.state.points.map((item, key) => {
      let Item = function(props) {
        let { connectDragSource, connectDropTarget, removePoint, isDragging, isOver } = props;
        return connectDropTarget(connectDragSource(
          <div className={ isDragging ? `point-item ${isOver ? 'over' : ''} drag-active` : 'point-item'}>
            <span>{`Точка маршрута № ${key+1}`}</span>
            <div onClick={removePoint.bind(null, key)} className="remover"></div>
          </div>
        ));
      }
      let NewItem = DragSource('point', source, collectSource)(Item);
      NewItem = DropTarget('point', target , collectTarget)(NewItem);

      return (
        <React.Fragment key={key}>
          <NewItem replacePoints={this.replacePoints} index={key} removePoint={this.removePoint} />
        </React.Fragment>
      );

    });

    return (
      <DragDropContextProvider backend={HTML5Backend}>
        <div className={`app-wrapper ${loaded ? 'loaded' : ''}`}>
          <div className="controls-wrapper">
            <DropDownList
              onChange={this.searchPoint}
              onSelect={this.selectPoint}
              list={this.state.searchResults}
              onClose = {this.onClose}
            />
            <div className="route-list">
              {Items}
            </div>
          </div>
            <YMaps>
                <Map onLoad={this.setLoaded} className="map" style={{width: '75%', height: '500px'}} state={{...{}, ...{
                  center: this.state.defaultPos,
                  zoom: 4,
                }}} >
                  {this.state.points.map((item, key) => {
                    return <Point
                      key={key}
                      index={key}
                      name={item.GeoObject.metaDataProperty.GeocoderMetaData.Address.formatted}
                      coordinates={item.point}
                      onDragEnd={this.onDragEnd}
                      onDrag={this.handleDrag.bind(null, key)}
                     />
                  })}
                  <Line coordinates={points.map(item => item.point)} />
                </Map>
            </YMaps>
          </div>
        </DragDropContextProvider>
    )
  }
}

export default App;