import React, {Component} from 'react';
import { Placemark } from 'react-yandex-maps';

export default class Point extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { onDrag, coordinates, index, onDragEnd, name } = this.props;

    return (
      <Placemark
        key = {index}
        onDrag = {onDrag}
        onDragEnd = {onDragEnd}
        geometry={{
          type: 'Point',
          coordinates: coordinates,
        }}
        options = {{
          draggable: true,
        }}
        properties = {{
          balloonContent: name
        }}
        modules = {
          ['geoObject.addon.balloon']
        }
      />
    );
  }
}