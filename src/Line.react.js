import React, {Component} from 'react';
import { GeoObject } from 'react-yandex-maps';

export default class Line extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
      const { coordinates } = this.props;

      return (
        <GeoObject
          geometry={{
            type: 'LineString',
            coordinates: coordinates,
          }}
          options={{
            geodesic: false,
            strokeWidth: 5,
            strokeColor: '#F008',
          }}
        />
      );
    }
}