export const get = (data) => {
	let url = new URL(data.url);
	let params = data.parameters ? data.parameters : {};
	Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
	return fetch(url.href, {
		mode: 'cors',
	}).then(response => {
		return response.json();
	}).then(json => {
		return json;
	})
	.catch(error => {
		console.log('error', error);
	});
}