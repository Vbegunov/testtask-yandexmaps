import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
const data = [
	{
		id: 1,
		name: 'Отдел кадров',
		children: [{
			id: 3,
			name: 'Москва'
		}]
	},
	{
		id: 2,
		name: 'Отдел разработки'
	},
	{
		id: 4,
		name: 'Отдел разработки - 2',
		children: [
			{
				id: 5,
				name: 'Отдел разработки 2.1',
			},
			{
				id: 6,
				name: 'Отдел разработки 2.2',
				children: [
					{
						id: 10,
						name: 'Asdasdasd'
					}
				]
			}
		]
	},
	{
		id: 7,
		name: 'Отдел разработки - 3'
	}
];

// 1
//   1.3
// 2
// 4
//  4.5
//    

ReactDOM.render(<App data={data} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
