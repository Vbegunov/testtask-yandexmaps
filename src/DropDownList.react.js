import React from 'react';

export default class DropDown extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: '',
    };

    this.openList = this.openList.bind(this);
    this.closeList = this.closeList.bind(this);
    this.selectItem = this.selectItem.bind(this);
    this.cleanUp = this.cleanUp.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  openList(e) {
    e.preventDefault();
    e.stopPropagation();
    const { value } = e.target;
    const { onChange } = this.props;
    if(value) {
      onChange(value);
    }
  }

  closeList() {
    const { onClose } = this.props;
    onClose();
  }

  selectItem(item) {
    // this.setState({selected: item.name});
    const { onSelect } = this.props;
    onSelect(item);
  }
  
  cleanUp(e) {
    e.stopPropagation();
    this.setState({selected: ''});
  }

  onChange(e) {
    const { onChange } = this.props;
    const { value } = e.target;
    if(value) {
      onChange(value);
    }
  }

  render() {
    const { list } = this.props;
    return (
      <div className="drop-down">
        <div className="controls">
         <input
          placeholder="Выберите из списка"
          className="dd-input"
          onFocus={this.openList}
          onBlur={this.closeList}
          onChange={this.onChange}
          />
          {list.length > 0 ?
             <div className="list-wrapper">
              <ListItems
                onSelectItem={this.selectItem}
                list={list} />
            </div>
          :
            null
          }
        </div>
      </div>
    );
  }
}

let ListItems = ({list, isChild, onSelectItem}) => (
  <div className={isChild ? 'child-item items-list' : 'items-list'}>
    {list.map((item, key) => {
        return (
          <div
            key={key}
            onMouseDown={onSelectItem.bind(null, item)}
            className={isChild ? 'child-item item' : 'item'}>
              {`${item.GeoObject.metaDataProperty.GeocoderMetaData.text}`}
          </div>
        )
    })}
  </div>
);